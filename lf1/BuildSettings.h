//
//  Created by Chris Rivers on 22/02/2014.
//  Copyright (c) 2014 Lofionic. All rights reserved.
//

#ifndef LF1_BuildSettings_h
#define LF1_BuildSettings_h

#define USE_ANALOG 1
#define ANALOG_HARMONICS 15
#define ROTARY_CONTROLS 0


#endif